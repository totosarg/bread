# Installation instructions

1. `git clone git@bitbucket.org:totosarg/bread.git`
2. Navigate to the directory where you cloned the repository.
3. Run `npm install`
4. To run tests execute `npm test` in the project directory.


# Local environment
1. Node version: v13.8.0
