class Authorization {
    constructor(permissions, users) {
      this.permissions = permissions
      this.users = users
    }
    
    listPermissions(userId) {
      let user = this.users.find(u => u.id === userId);
      let userPermissions = [];
      this.permissions.forEach(permission => {
        if (user.roles.includes(permission.role) && permission.active) {
            userPermissions.push(permission.name);
        }
      });
      return userPermissions;
    }
  
    checkPermitted(permissionName, userId) {
      let userPermissions = this.listPermissions(userId);
      if (userPermissions.includes(permissionName)) {
          return true;
      }
      return false;
    }
  }

 module.exports = Authorization;
